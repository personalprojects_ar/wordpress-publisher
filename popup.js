function dataURLToBlob(dataURL) {
  var BASE64_MARKER = ';base64,';
  if (dataURL.indexOf(BASE64_MARKER) == -1) {
    var parts = dataURL.split(',');
    var contentType = parts[0].split(':')[1];
    var raw = decodeURIComponent(parts[1]);

    return new Blob([raw], {type: contentType});
  }

  var parts = dataURL.split(BASE64_MARKER);
  var contentType = parts[0].split(':')[1];
  var raw = window.atob(parts[1]);
  var rawLength = raw.length;

  var uInt8Array = new Uint8Array(rawLength);

  for (var i = 0; i < rawLength; ++i) {
    uInt8Array[i] = raw.charCodeAt(i);
  }

  return new Blob([uInt8Array], {type: contentType});
}

function filterContent(content,url_page,category,media){
  // Getting the domain
  var urlParts = url_page.replace('http://','').replace('https://','').split(/[/?#]/);
  var title  = '';
  var domain = urlParts[0];
  switch(domain) {
      case "acento.com.do":
        title = urlParts[3];
      break;
      case "z101digital.com":
        title = urlParts[2];;
      break;
      case "www.diariolibre.com":
        title = urlParts[2];;
      break;
      case "www.listindiario.com":
        title = urlParts[6];;
      break;
      default:
        title = url;
      break;
  }

  // Cleaning the article title
  title = title.replace(new RegExp('-', 'g'), ' ');
  
  // Getting the auth token
  var auth_data = {username:'argeni','password':'Entrada01'};
  $.ajax({
      method: "POST",
      url:"http://www.soyerrede.com/wp-json/jwt-auth/v1/token",
      data: auth_data,
      success : function( response ) {
          if(media != "")
          {
            var blob = dataURLToBlob(media);
            var apiData = new FormData();
            apiData.append( "file", blob);
            console.log(apiData);
            $.ajax({
                      method: "POST",
                      url:"http://www.soyerrede.com/wp-json/wp/v2/media",
                      dataType: 'json', //return response as json
                      mimeType: "multipart/form-data",
                      cache: false,
                      contentType: false,
                      processData: false,
                      data: apiData,
                      headers: { 'Authorization': 'Bearer ' + response.token,'Content-Disposition':'attachment; filename='+media},
                      success : function( response_media ) {
                        console.log( response_media );
                      }
              });
          }

          var data = {
              title: title,
              content: content,
              categories: [category]
          };
          // Saving the post
          // $.ajax({
          //     method: "POST",
          //     url:"http://www.soyerrede.com/wp-json/wp/v2/posts",
          //     data: data,
          //     headers: { 'Authorization': 'Bearer ' + response.token },
          //     success : function( response_post ) {
          //       console.log( response_post );
          //     },
          //     fail : function( response_post ) {
          //       console.log( response_post );
          //     }
          // });
      },
      fail : function( response ) {
        console.log( response );
      }
  });
}

$(document).ready(function(){

  // Getting the categories 
  $.ajax({
    url:"http://www.soyerrede.com/wp-json/wp/v2/categories",
    method:"GET",
    success:function(data){
      for(var c in data) {
        var current = data[c];
        if(current.name != " ") {
            var tempOption = "<option value="+current.id+">"+current.name+"</option>";
            $("#txtCategoria").append(tempOption);
        }
      }
    }
  });

   $("#btn_send").click(function(){
      // Validating if the category is set
      var categoria = $("#txtCategoria").val();
      if( categoria != '') {
          var queryInfo = {
            active: true,
            currentWindow: true
          };
          chrome.tabs.query(queryInfo, function(tabs) {

            var tab = tabs[0];
            var url = tab.url;
            chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                  chrome.tabs.sendMessage(tab.id, {method: "getText",url:url}, function(response) {
                      if(response.method=="getText"){
                          filterContent(response.data,url,categoria,response.media);
                      }
                  });
            });
          });
      }
      else {

      }
   });
});


